const { NODE_ENV = 'development' } = process.env;
const Sequelize = require('sequelize');
const config = require('../config/database')[NODE_ENV];

let db;
const connectDB = async () => {
  const client = new Sequelize(config);
  try {
    await client.authenticate();
    console.log('Database: Connection has been established successfully.');
  } catch (error) {
    console.error('Database:', error);
  }

  db = client;
};

const getDB = () => {
  return db;
};

const sequelize = () => {
  return db;
};

const beginTransaction = () => {
  return db.transaction();
};

module.exports = { connectDB, getDB, sequelize, beginTransaction };
