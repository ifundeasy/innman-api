# innman

## installation

Create database with `innman` name

```bash
mysqladmin -u root -p create innman
```

Clone this repository

```bash
git clone https://gitlab.com/ifundeasy/innman-api.git innman-api
cd innman-api
```

Create and edit `.env` file as you need (note: for test or development only, not in production).

```bash
cp .env.example .env
vi .env
```

Install node module

```bash
npm i
```

Sync model to database

```bash
# this command will build database tables and example data
npm run syncdb
```

## run

Running a web server

```bash
npm run dev
```

Web server run on `http://localhost:3000`

## demo

Open postman application, then import `postman/innman.postman_collection.json`. You can manage hotel, room, and reservation from example api endpoint imported.

Happy managing!
