const { Op } = require('sequelize');
const moment = require('moment');
const Hotel = require('../models/hotel');
const { getDB, beginTransaction } = require('../libs/connection');

exports.addOne = async (req, res, next) => {
  try {
    let { hotel_name, address } = req.body;
    const dbClient = getDB();
    const hotel = Hotel(dbClient);
    const result = await hotel.create({ hotel_name, address });

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.getAll = async (req, res, next) => {
  try {
    let { hotel_name = '', address = '', limit, offset } = req.query;
    limit = parseInt(limit) > -1 ? parseInt(limit) : 10;
    offset = parseInt(offset) > -1 ? parseInt(offset) : 0;

    if (hotel_name) hotel_name = { hotel_name: { [Op.like]: `%${hotel_name}%` } };
    if (address) address = { address: { [Op.like]: `%${address}%` } };

    const dbClient = getDB();
    const hotel = Hotel(dbClient);
    const orOperation = {
      ...hotel_name,
      ...address
    };
    const condition = !Object.keys(orOperation).length
      ? {}
      : {
          [Op.or]: orOperation
        };
    const result = await hotel.findAndCountAll({
      limit,
      offset,
      where: condition
    });

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.getById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    const dbClient = getDB();
    const hotel = Hotel(dbClient);
    const result = await hotel.findByPk(id);

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.updateById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    let { hotel_name = '', address = '' } = req.body;

    if (hotel_name) hotel_name = { hotel_name };
    if (address) address = { address };

    const dbClient = getDB();
    const hotel = Hotel(dbClient);
    const result = await hotel.update(
      { ...hotel_name, ...address, updated_dt: moment().format() },
      { where: { id } }
    );

    return res.json({
      count: result[0]
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.deleteById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    const dbClient = getDB();
    const hotel = Hotel(dbClient);
    const result = await hotel.destroy({ where: { id } });

    return res.json({
      count: result
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};
