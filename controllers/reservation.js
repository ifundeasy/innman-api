const { Op, QueryTypes } = require('sequelize');
const moment = require('moment');
const Hotel = require('../models/hotel');
const Room = require('../models/room');
const Reservation = require('../models/reservation');
const Stay = require('../models/stay');
const StayRoom = require('../models/stayRoom');
const { getDB, beginTransaction } = require('../libs/connection');

exports.addOne = async (req, res, next) => {
  try {
    let { order_id, hotel_id, room_id, checkin_date, customer_name, checkout_date } = req.body;
    const dbClient = getDB();
    const reservation = Reservation(dbClient);
    const result = await reservation.create({
      order_id,
      hotel_id,
      room_id,
      customer_name,
      checkin_date,
      checkout_date
    });

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.getAll = async (req, res, next) => {
  try {
    let {
      order_id = '',
      hotel_id = null,
      room_id = null,
      customer_name,
      checkin_date,
      checkout_date,
      limit,
      offset
    } = req.query;
    limit = parseInt(limit) > -1 ? parseInt(limit) : 10;
    offset = parseInt(offset) > -1 ? parseInt(offset) : 0;

    if (order_id) order_id = { order_id: { [Op.like]: `%${order_id}%` } };
    if (hotel_id) hotel_id = { hotel_id };
    if (room_id) room_id = { room_id };
    if (customer_name) customer_name = { customer_name: { [Op.like]: `%${customer_name}%` } };
    if (checkin_date) checkin_date = { checkin_date: { [Op.gte]: checkin_date } };
    if (checkout_date) checkout_date = { checkout_date: { [Op.lte]: checkout_date } };

    const dbClient = getDB();
    const hotel = Hotel(dbClient);
    const room = Room(dbClient);
    const reservation = Reservation(dbClient);
    hotel.hasMany(room, { foreignKey: 'hotel_id' });
    room.hasMany(reservation, { foreignKey: 'room_id' });
    reservation.belongsTo(room, { foreignKey: 'room_id' });
    reservation.belongsTo(hotel, { foreignKey: 'hotel_id' });

    const andOperation = {
      ...order_id,
      ...hotel_id,
      ...room_id,
      ...customer_name,
      ...checkin_date,
      ...checkout_date
    };
    const condition = !Object.keys(andOperation).length
      ? {}
      : {
          [Op.and]: andOperation
        };
    const result = await reservation.findAndCountAll({
      limit,
      offset,
      where: condition,
      include: [
        {
          model: room,
          required: true
        },
        {
          model: hotel,
          required: true
        }
      ]
    });

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.getById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    const dbClient = getDB();
    const hotel = Hotel(dbClient);
    const room = Room(dbClient);
    const reservation = Reservation(dbClient);
    hotel.hasMany(room, { foreignKey: 'hotel_id' });
    room.hasMany(reservation, { foreignKey: 'room_id' });
    reservation.belongsTo(room, { foreignKey: 'room_id' });
    reservation.belongsTo(hotel, { foreignKey: 'hotel_id' });

    const result = await reservation.findAndCountAll({
      where: { id },
      include: [
        {
          model: room,
          required: true
        },
        {
          model: hotel,
          required: true
        }
      ]
    });

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.updateById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    let {
      order_id = '',
      hotel_id = '',
      room_id = '',
      customer_name = '',
      checkin_date = '',
      checkout_date = ''
    } = req.body;

    if (order_id) order_id = { order_id };
    if (hotel_id) hotel_id = { hotel_id };
    if (room_id) room_id = { room_id };
    if (customer_name) customer_name = { customer_name };
    if (checkin_date) checkin_date = { checkin_date };
    if (checkout_date) checkout_date = { checkout_date };

    const dbClient = getDB();
    const reservation = Reservation(dbClient);
    const result = await reservation.update(
      {
        ...order_id,
        ...hotel_id,
        ...room_id,
        ...customer_name,
        ...checkin_date,
        ...checkout_date,
        updated_dt: moment().format()
      },
      { where: { id } }
    );

    return res.json({
      count: result[0]
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.deleteById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    const dbClient = getDB();
    const reservation = Reservation(dbClient);
    const result = await reservation.destroy({ where: { id } });

    return res.json({
      count: result
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.getAvailable = async (req, res, next) => {
  try {
    let { checkin_date, checkout_date, inner } = req.query;

    checkin_date = moment(checkin_date);
    if (checkin_date.toString() === 'Invalid date') {
      checkin_date = moment().format();
    }
    checkin_date = checkin_date.format('YYYY-MM-DD');

    if (!checkout_date) checkout_date = checkin_date;
    else {
      checkout_date = moment(checkout_date);
      if (checkout_date.toString() === 'Invalid date') {
        checkout_date = moment().format();
      }
    }
    checkout_date = checkout_date.format('YYYY-MM-DD');

    const dbClient = getDB();
    let result = await dbClient.query(
      `
      SELECT MAX(hotel_name) hotel_name, count(room.id) available
      FROM room
      JOIN hotel ON hotel.id = room.hotel_id
      WHERE room.id NOT IN (
        SELECT room_id FROM reservation
        WHERE
          COALESCE('${checkin_date}' BETWEEN checkin_date AND checkout_date, TRUE)
          ${inner === 'false' ? 'AND' : 'OR'}
          COALESCE('${checkout_date}' BETWEEN checkin_date AND checkout_date, TRUE)  
      ) AND room_status = 'available'
      GROUP BY hotel.id;
      `,
      { type: QueryTypes.SELECT }
    );

    result = result.filter(function (el) {
      return el.hotel_name && el.available;
    });

    return res.json({
      count: result.length,
      rows: result
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.processReservation = async (req, res, next) => {
  let { order_id, hotel_id, room_id, checkin_date, customer_name, checkout_date } = req.body;
  let transaction = await beginTransaction();

  const dbClient = getDB();
  const reservation = Reservation(dbClient);
  try {
    const [reservationData, isNewReservation] = await reservation.findOrCreate({
      where: {
        order_id,
        hotel_id,
        room_id,
        checkin_date,
        customer_name,
        checkout_date
      },
      defaults: {
        order_id,
        hotel_id,
        room_id,
        checkin_date,
        customer_name,
        checkout_date
      },
      transaction
    });

    const stay = Stay(dbClient);
    const reservation_id = reservationData.id;
    const [stayData, isNewStay] = await stay.findOrCreate({
      where: {
        reservation_id,
        room_id,
        guest_name: reservationData.customer_name
      },
      defaults: {
        reservation_id,
        room_id,
        guest_name: reservationData.customer_name
      },
      transaction
    });

    const stayRoom = StayRoom(dbClient);
    const diffDay = moment(checkin_date).diff(moment(checkout_date), 'days');
    const dayCounts = Math.abs(diffDay) + 1;
    const stay_id = stayData.id;
    const stayRoomDatas = [];
    for (let i = 0; i < dayCounts; i++) {
      const date = moment(checkin_date).add(i, 'd').format();
      const [stayRoomData, isNewStayRoom] = await stayRoom.findOrCreate({
        where: {
          stay_id,
          room_id,
          date
        },
        defaults: {
          stay_id,
          room_id,
          date
        },
        transaction
      });
      stayRoomDatas.push(stayRoomData.dataValues);
    }

    await transaction.commit();

    return res.json({
      ...reservationData.dataValues,
      stay_model: {
        ...stayData.dataValues,
        stayRoom_model: stayRoomDatas
      }
    });
  } catch (e) {
    await transaction.rollback();
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};
