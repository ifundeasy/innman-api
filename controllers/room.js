const { Op } = require('sequelize');
const moment = require('moment');
const Room = require('../models/room');
const RoomPrice = require('../models/roomPrice');
const { getDB, beginTransaction } = require('../libs/connection');

exports.addOne = async (req, res, next) => {
  let { hotel_id, room_number, room_status, price, start_date, end_date } = req.body;
  let transaction = await beginTransaction();

  const isIncludePrice = price && start_date && end_date;
  const dbClient = getDB();
  const room = Room(dbClient);
  const roomPrice = RoomPrice(dbClient);
  try {
    const [roomData, isNewRoom] = await room.findOrCreate({
      where: {
        hotel_id,
        room_number
      },
      defaults: {
        room_status
      },
      transaction
    });

    let roomPriceData, isNewRoomPrice;
    if (isIncludePrice) {
      const room_id = roomData.id;
      [roomPriceData, isNewRoomPrice] = await roomPrice.findOrCreate({
        where: {
          hotel_id,
          room_id,
          end_date
        },
        defaults: {
          hotel_id,
          room_id,
          price,
          start_date,
          end_date
        },
        transaction
      });
    }

    await transaction.commit();

    if (!isIncludePrice) {
      return res.json(roomData);
    }

    return res.json({
      ...roomData.dataValues,
      price: roomPriceData.price,
      start_date: roomPriceData.start_date,
      end_date: roomPriceData.end_date
    });
  } catch (e) {
    await transaction.rollback();
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.getAll = async (req, res, next) => {
  try {
    let {
      room_number = '',
      room_status = '',
      start_date = '',
      end_date = '',
      price,
      limit,
      offset
    } = req.query;
    price = parseInt(price) > -1 ? parseInt(price) : 0;
    limit = parseInt(limit) > -1 ? parseInt(limit) : 10;
    offset = parseInt(offset) > -1 ? parseInt(offset) : 0;

    if (room_number) room_number = { room_number };
    if (room_status) room_status = { room_status };
    if (start_date) start_date = { '$roomPrice_models.start_date$': { [Op.gte]: start_date } };
    if (end_date) end_date = { '$roomPrice_models.end_date$': { [Op.lte]: end_date } };
    if (price) price = { '$roomPrice_models.price$': { [Op.gte]: price } };

    const dbClient = getDB();
    const room = Room(dbClient);
    const roomPrice = RoomPrice(dbClient);
    room.hasMany(roomPrice, { foreignKey: 'room_id' });
    // roomPrice.belongsTo(room, { foreignKey: 'room_id' }); // this will make outer left join

    const andOperation = {
      ...room_number,
      ...room_status
    };
    const condition = !Object.keys(andOperation).length
      ? {}
      : {
          [Op.and]: andOperation
        };
    const andOperationChildren = {
      ...start_date,
      ...end_date,
      ...price
    };
    const childCondition = !Object.keys(andOperationChildren).length
      ? {}
      : {
          [Op.and]: andOperationChildren
        };
    const result = await room.findAndCountAll({
      limit,
      offset,
      where: condition,
      include: {
        model: roomPrice,
        required: false,
        where: childCondition
      }
    });

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.getById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    const dbClient = getDB();
    const room = Room(dbClient);
    const roomPrice = RoomPrice(dbClient);
    room.hasMany(roomPrice, { foreignKey: 'room_id' });
    // roomPrice.belongsTo(room, { foreignKey: 'room_id' }); // this will make outer left join

    const result = await room.findAndCountAll({
      where: { id },
      include: {
        model: roomPrice,
        required: false
      }
    });

    return res.json(result);
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.updateById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    let { hotel_id = '', room_number = '', room_status = '' } = req.body;

    if (hotel_id) hotel_id = { hotel_id };
    if (room_number) room_number = { room_number };
    if (room_status) room_status = { room_status };

    const dbClient = getDB();
    const room = Room(dbClient);
    const result = await room.update(
      { ...hotel_id, ...room_number, ...room_status, updated_dt: moment().format() },
      { where: { id } }
    );

    return res.json({
      count: result[0]
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.updatePriceById = async (req, res, next) => {
  try {
    const { id = null, priceId = null } = req.params;
    let { price = '', start_date = '', end_date = '' } = req.body;

    if (price) price = { price };
    if (start_date) start_date = { start_date };
    if (end_date) end_date = { end_date };

    const dbClient = getDB();
    const roomPrice = RoomPrice(dbClient);
    const result = await roomPrice.update(
      { ...price, ...start_date, ...end_date, updated_dt: moment().format() },
      { where: { id: priceId, room_id: id } }
    );

    return res.json({
      count: result[0]
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.deleteById = async (req, res, next) => {
  try {
    const { id = null } = req.params;
    const dbClient = getDB();
    const room = Room(dbClient);
    const result = await room.destroy({ where: { id } });

    return res.json({
      count: result
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};

exports.deletePriceById = async (req, res, next) => {
  try {
    const { id = null, priceId = null } = req.params;
    const dbClient = getDB();
    const roomPrice = RoomPrice(dbClient);
    const result = await roomPrice.destroy({ where: { id: priceId, room_id: id } });

    return res.json({
      count: result
    });
  } catch (e) {
    return res.status(500).send({
      error: 501,
      message: e.message
    });
  }
};
