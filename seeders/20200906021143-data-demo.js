'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.bulkInsert(
        'hotel',
        [
          {
            id: 1,
            hotel_name: 'The Media Hotel',
            address: 'Jl. Gn. Sahari, Jakarta'
          },
          {
            id: 2,
            hotel_name: 'Hotel Ciputra Jakarta',
            address: 'Letjen S. Parman, Tanjung Duren, Jakarta'
          }
        ],
        { transaction }
      );
      await queryInterface.bulkInsert(
        'room',
        [
          {
            id: 1,
            hotel_id: 1,
            room_number: 1,
            room_status: 'available'
          },
          {
            id: 2,
            hotel_id: 1,
            room_number: 2,
            room_status: 'out of service'
          },
          {
            id: 3,
            hotel_id: 1,
            room_number: 3,
            room_status: 'available'
          },
          {
            id: 4,
            hotel_id: 2,
            room_number: 1,
            room_status: 'available'
          },
          {
            id: 5,
            hotel_id: 2,
            room_number: 2,
            room_status: 'out of service'
          }
        ],
        { transaction }
      );
      await queryInterface.bulkInsert(
        'roomPrice',
        [
          {
            id: 1,
            hotel_id: 1,
            room_id: 1,
            price: 350000,
            start_date: '2020-09-05',
            end_date: '2021-09-05'
          },
          {
            id: 2,
            hotel_id: 1,
            room_id: 2,
            price: 450000,
            start_date: '2020-09-05',
            end_date: '2021-09-05'
          },
          {
            id: 3,
            hotel_id: 1,
            room_id: 3,
            price: 550000,
            start_date: '2020-09-05',
            end_date: '2021-09-05'
          },
          {
            id: 4,
            hotel_id: 2,
            room_id: 4,
            price: 350000,
            start_date: '2020-09-05',
            end_date: '2021-09-05'
          },
          {
            id: 5,
            hotel_id: 2,
            room_id: 5,
            price: 450000,
            start_date: '2020-09-05',
            end_date: '2021-09-05'
          }
        ],
        { transaction }
      );
      await queryInterface.bulkInsert(
        'reservation',
        [
          {
            id: 1,
            order_id: 'Order001',
            hotel_id: 1,
            room_id: 1,
            customer_name: 'Kamal',
            checkin_date: '2020-09-06',
            checkout_date: '2020-09-06'
          },
          {
            id: 2,
            order_id: 'Order002',
            hotel_id: 1,
            room_id: 1,
            customer_name: 'Amir',
            checkin_date: '2020-09-08',
            checkout_date: '2020-09-10'
          }
        ],
        { transaction }
      );
      await queryInterface.bulkInsert(
        'stay',
        [
          {
            id: 1,
            reservation_id: 1,
            room_id: 1,
            guest_name: 'Kamal'
          },
          {
            id: 2,
            reservation_id: 2,
            room_id: 1,
            guest_name: 'Amir'
          }
        ],
        { transaction }
      );
      await queryInterface.bulkInsert(
        'stayRoom',
        [
          {
            id: 1,
            stay_id: 1,
            room_id: 1,
            date: '2020-09-06'
          },
          {
            id: 2,
            stay_id: 2,
            room_id: 1,
            date: '2020-09-08'
          },
          {
            id: 3,
            stay_id: 2,
            room_id: 1,
            date: '2020-09-09'
          },
          {
            id: 4,
            stay_id: 2,
            room_id: 1,
            date: '2020-09-10'
          }
        ],
        { transaction }
      );
      await transaction.commit();
    } catch (e) {
      console.error(e);
      await transaction.rollback();
    }
  },

  down: async (queryInterface, Sequelize) => {
    let transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.bulkDelete('stayRoom', {}, { transaction });
      await queryInterface.bulkDelete('stay', {}, { transaction });
      await queryInterface.bulkDelete('reservation', {}, { transaction });
      await queryInterface.bulkDelete('roomPrice', {}, { transaction });
      await queryInterface.bulkDelete('room', {}, { transaction });
      await queryInterface.bulkDelete('hotel', {}, { transaction });
      await transaction.commit();
    } catch (e) {
      console.error(e);
      await transaction.rollback();
    }
  }
};
