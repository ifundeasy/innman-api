require('dotenv').config();

const { NODE_ENV = 'development' } = process.env;

module.exports = {
  dbOptions: require('./database')[NODE_ENV]
};
