const router = require('express').Router();
const RoomController = require('../controllers/room');

router.post('/', RoomController.addOne);
router.get('/', RoomController.getAll);
router.get('/:id', RoomController.getById);
router.put('/:id', RoomController.updateById);
router.put('/:id/price/:priceId', RoomController.updatePriceById);
router.delete('/:id', RoomController.deleteById);
router.delete('/:id/price/:priceId', RoomController.deletePriceById);

module.exports = router;
