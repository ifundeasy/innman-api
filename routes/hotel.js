const router = require('express').Router();
const HotelController = require('../controllers/hotel');

router.post('/', HotelController.addOne);
router.get('/', HotelController.getAll);
router.get('/:id', HotelController.getById);
router.put('/:id', HotelController.updateById);
router.delete('/:id', HotelController.deleteById);

module.exports = router;
