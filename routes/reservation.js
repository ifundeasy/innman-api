const router = require('express').Router();
const ReservationController = require('../controllers/reservation');

router.post('/', ReservationController.addOne);
router.post('/:id/process', ReservationController.processReservation);
router.get('/', ReservationController.getAll);
router.get('/available', ReservationController.getAvailable);
router.get('/:id', ReservationController.getById);
router.put('/:id', ReservationController.updateById);
router.delete('/:id', ReservationController.deleteById);

module.exports = router;
