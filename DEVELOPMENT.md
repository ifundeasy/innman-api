# innman

## schema

When you do changing any table on database please create migration file, for example below creating new table with name `logging`:

```bash
npx sequelize-cli migration:generate --name create-logging
```

Then update `migrations/xxxxxxxxxxxxxx-create-logging.js` file as you need for migration up and down.

After that sync database with this command

```bash
npm run syncdb
```

> visit `package.json` scripts value key for complete information

## web server

For auto restart after editing source code run this command:

```bash
npm run watch:dev
```
