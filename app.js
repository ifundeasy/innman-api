'use strict';

/**
 * Module dependencies.
 */
const path = require('path');
const morgan = require('morgan');
const express = require('express');
require('express-async-errors');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const { connectDB } = require('./libs/connection.js');

/**
 * Create Express app
 */
const app = express();
const rawBodySaver = function (req, res, buf, encoding) {
  if (buf && buf.length) {
    req.rawBody = buf.toString(encoding || 'utf8');
  }
};

/**
 * This is starter middlewares
 */
app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.raw({ verify: rawBodySaver, type: () => true }));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use((req, res, next) => {
  const { headers, query } = req;
  const body = req.rawBody || JSON.stringify(req.body);

  console.log('');
  console.log(req.method, req.url);
  // console.log('HEADER:', JSON.stringify(headers));
  if (Object.keys(query).length) console.log('QUERY:', JSON.stringify(query));
  if (body) console.log('BODY:', body);
  console.log('...');
  next();
});

/**
 * Custom routers and bussiness logic goes here
 */
app.use('/', require('./routes'));
app.use('/hotel', require('./routes/hotel'));
app.use('/room', require('./routes/room'));
app.use('/reservation', require('./routes/reservation'));

/**
 * This is fallback middlewares
 */
app.use(function (req, res, next) {
  res.end("Didn't match a route!");
  next();
});
app.use(function (err, req, res, next) {
  res.status(500).send({ error: 0, message: err.message });
});

connectDB();

module.exports = app;
