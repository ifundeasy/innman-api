const {
  DataTypes
} = require('sequelize');

module.exports = sequelize => {
  const attributes = {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: "id"
    },
    reservation_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "reservation_id",
      references: {
        key: "id",
        model: "reservation_model"
      }
    },
    room_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "room_id",
      references: {
        key: "id",
        model: "room_model"
      }
    },
    guest_name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "",
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "guest_name"
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "created_dt"
    },
    updated_dt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "updated_dt"
    }
  };
  const options = {
    tableName: "stay",
    comment: "",
    indexes: [{
      name: "stay_reservation_id_reservation_fk",
      unique: false,
      type: "BTREE",
      fields: ["reservation_id"]
    }, {
      name: "stay_room_id_room_fk",
      unique: false,
      type: "BTREE",
      fields: ["room_id"]
    }]
  };
  const StayModel = sequelize.define("stay_model", attributes, options);
  return StayModel;
};