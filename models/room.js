const {
  DataTypes
} = require('sequelize');

module.exports = sequelize => {
  const attributes = {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: "id"
    },
    hotel_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "hotel_id",
      references: {
        key: "id",
        model: "hotel_model"
      }
    },
    room_number: {
      type: DataTypes.INTEGER(6).UNSIGNED,
      allowNull: false,
      defaultValue: "0",
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "room_number"
    },
    room_status: {
      type: DataTypes.ENUM('available', 'out of service'),
      allowNull: false,
      defaultValue: "available",
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "room_status"
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "created_dt"
    },
    updated_dt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "updated_dt"
    }
  };
  const options = {
    tableName: "room",
    comment: "",
    indexes: [{
      name: "hotel_id_room_number",
      unique: true,
      type: "BTREE",
      fields: ["hotel_id", "room_number"]
    }]
  };
  const RoomModel = sequelize.define("room_model", attributes, options);
  return RoomModel;
};