const {
  DataTypes
} = require('sequelize');

module.exports = sequelize => {
  const attributes = {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      comment: null,
      field: "id"
    },
    stay_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "stay_id",
      references: {
        key: "id",
        model: "stay_model"
      }
    },
    room_id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "room_id",
      references: {
        key: "id",
        model: "room_model"
      }
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "date"
    },
    created_dt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "created_dt"
    },
    updated_dt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
      primaryKey: false,
      autoIncrement: false,
      comment: null,
      field: "updated_dt"
    }
  };
  const options = {
    tableName: "stayRoom",
    comment: "",
    indexes: [{
      name: "stayRoom_stay_id_stay_fk",
      unique: false,
      type: "BTREE",
      fields: ["stay_id"]
    }, {
      name: "stayRoom_room_id_room_fk",
      unique: false,
      type: "BTREE",
      fields: ["room_id"]
    }]
  };
  const StayRoomModel = sequelize.define("stayRoom_model", attributes, options);
  return StayRoomModel;
};