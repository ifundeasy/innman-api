'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('reservation', {
      id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      order_id: {
        type: Sequelize.STRING(30),
        allowNull: false
      },
      hotel_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'hotel',
          key: 'id'
        }
      },
      room_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'room',
          key: 'id'
        }
      },
      customer_name: {
        type: Sequelize.STRING(50),
        allowNull: false,
        defaultValue: ''
      },
      checkin_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      checkout_date: {
        type: Sequelize.DATEONLY
      },
      created_dt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_dt: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('reservation');
  }
};
