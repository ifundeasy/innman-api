'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    // ? queryInterface.addColumn 'after' keyword doesn't work
    queryInterface.sequelize.query(
      'ALTER TABLE `roomPrice` ADD `end_date` DATE  NOT NULL  AFTER `start_date`;'
    );
  },

  down: async (queryInterface, Sequelize) => {
    // ? queryInterface.removeColumn doesn't work
    queryInterface.sequelize.query('ALTER TABLE `roomPrice` DROP `end_date`;');
  }
};
