'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('stayRoom', {
      id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      stay_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'stay',
          key: 'id'
        }
      },
      room_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'room',
          key: 'id'
        }
      },
      date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      created_dt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_dt: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('stayRoom');
  }
};
