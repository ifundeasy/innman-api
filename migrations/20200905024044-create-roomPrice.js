'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('roomPrice', {
      id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      room_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'room',
          key: 'id'
        }
      },
      hotel_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'hotel',
          key: 'id'
        }
      },
      price: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        allowNull: false,
        defaultValue: 0
      },
      start_date: {
        type: Sequelize.DATEONLY,
        allowNull: false
      },
      created_dt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_dt: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('roomPrice');
  }
};
