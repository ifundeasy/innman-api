'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('stay', {
      id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      reservation_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'reservation',
          key: 'id'
        }
      },
      room_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'room',
          key: 'id'
        }
      },
      guest_name: {
        type: Sequelize.STRING(50),
        allowNull: false,
        defaultValue: ''
      },
      created_dt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_dt: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('stay');
  }
};
