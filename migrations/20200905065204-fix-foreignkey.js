'use strict';

async function addConstraint(queryInterface, transaction, events = {}) {
  // Note: add constrains for room table
  const roomConstraint = {
    fields: ['hotel_id'],
    type: 'foreign key',
    references: {
      table: 'hotel',
      field: 'id'
    },
    ...events
  };
  await queryInterface.addConstraint('room', roomConstraint, { transaction });

  // Note: add constrains for roomPrice table
  const roomPriceConstraints = [
    {
      fields: ['hotel_id'],
      type: 'foreign key',
      references: {
        table: 'hotel',
        field: 'id'
      },
      ...events
    },
    {
      fields: ['room_id'],
      type: 'foreign key',
      references: {
        table: 'room',
        field: 'id'
      },
      ...events
    }
  ];
  await queryInterface.addConstraint('roomPrice', roomPriceConstraints[0], { transaction });
  await queryInterface.addConstraint('roomPrice', roomPriceConstraints[1], { transaction });

  // Note: add constrains for reservation table
  const reservationConstraints = [
    {
      fields: ['hotel_id'],
      type: 'foreign key',
      references: {
        table: 'hotel',
        field: 'id'
      },
      ...events
    },
    {
      fields: ['room_id'],
      type: 'foreign key',
      references: {
        table: 'room',
        field: 'id'
      },
      ...events
    }
  ];
  await queryInterface.addConstraint('reservation', reservationConstraints[0], { transaction });
  await queryInterface.addConstraint('reservation', reservationConstraints[1], { transaction });

  // Note: add constrains for stay table
  const stayConstraints = [
    {
      fields: ['reservation_id'],
      type: 'foreign key',
      references: {
        table: 'reservation',
        field: 'id'
      },
      ...events
    },
    {
      fields: ['room_id'],
      type: 'foreign key',
      references: {
        table: 'room',
        field: 'id'
      },
      ...events
    }
  ];
  await queryInterface.addConstraint('stay', stayConstraints[0], { transaction });
  await queryInterface.addConstraint('stay', stayConstraints[1], { transaction });

  // Note: add constrains for stayRoom table
  const stayRoomConstraints = [
    {
      fields: ['stay_id'],
      type: 'foreign key',
      references: {
        table: 'stay',
        field: 'id'
      },
      ...events
    },
    {
      fields: ['room_id'],
      type: 'foreign key',
      references: {
        table: 'room',
        field: 'id'
      },
      ...events
    }
  ];
  await queryInterface.addConstraint('stayRoom', stayRoomConstraints[0], { transaction });
  await queryInterface.addConstraint('stayRoom', stayRoomConstraints[1], { transaction });
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let transaction = await queryInterface.sequelize.transaction();
    try {
      // Note: batch remove all constraints, just because no onDelete and onUpdate events.
      const tables = ['stayRoom', 'stay', 'reservation', 'roomPrice', 'room'];
      const tablesFkMap = await queryInterface.getForeignKeysForTables(tables, { transaction });
      for (let table in tablesFkMap) {
        for (let i in tablesFkMap[table]) {
          const constraintName = tablesFkMap[table][i];
          await queryInterface.removeConstraint(table, constraintName, { transaction });
        }
      }

      // Note: batch add constraints, for adding onDelete and onUpdate events.
      await addConstraint(queryInterface, transaction, {
        onDelete: 'cascade',
        onUpdate: 'cascade'
      });

      transaction.commit();
    } catch (e) {
      console.error(e);
      transaction.rollback();
    }
  },

  down: async (queryInterface, Sequelize) => {
    let transaction = await queryInterface.sequelize.transaction();
    try {
      // Note: batch remove all constraints.
      const tables = ['stayRoom', 'stay', 'reservation', 'roomPrice', 'room'];
      const tablesFkMap = await queryInterface.getForeignKeysForTables(tables, { transaction });
      for (let table in tablesFkMap) {
        for (let i in tablesFkMap[table]) {
          const constraintName = tablesFkMap[table][i];
          await queryInterface.removeConstraint(table, constraintName, { transaction });
        }
      }

      // Note: batch add constraints, w/o onDelete and onUpdate events.
      await addConstraint(queryInterface, transaction);
      transaction.commit();
    } catch (e) {
      console.error(e);
      transaction.rollback();
    }
  }
};
