'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('room', {
      id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        primaryKey: true,
        autoIncrement: true
      },
      hotel_id: {
        type: Sequelize.INTEGER(11).UNSIGNED,
        defaultValue: null,
        references: {
          model: 'hotel',
          key: 'id'
        }
      },
      room_number: {
        type: Sequelize.SMALLINT(6).UNSIGNED,
        allowNull: false,
        defaultValue: 0
      },
      room_status: {
        type: Sequelize.ENUM('available', 'out of service'),
        allowNull: false,
        defaultValue: 'available'
      },
      created_dt: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updated_dt: {
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('room');
  }
};
