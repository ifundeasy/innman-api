'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query(
        'ALTER TABLE hotel ADD CONSTRAINT hotel_name_address UNIQUE KEY (hotel_name, address);',
        { transaction }
      );
      await queryInterface.sequelize.query(
        'ALTER TABLE room ADD CONSTRAINT hotel_id_room_number UNIQUE KEY (hotel_id, room_number);',
        { transaction }
      );
      await queryInterface.sequelize.query(
        'ALTER TABLE roomPrice ADD CONSTRAINT hotel_id_room_id_end_date UNIQUE KEY (hotel_id, room_id, end_date);',
        { transaction }
      );
      await queryInterface.sequelize.query(
        'ALTER TABLE reservation ADD CONSTRAINT order_id_hotel_id_room_id UNIQUE KEY (order_id, hotel_id, room_id);',
        { transaction }
      );

      transaction.commit();
    } catch (e) {
      console.error(e);
      transaction.rollback();
    }
  },

  down: async (queryInterface, Sequelize) => {
    let transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query(
        'ALTER TABLE reservation DROP INDEX order_id_hotel_id_room_id',
        { transaction }
      );

      // ! Don't know why? this query line will throw error:
      // ! Cannot drop index 'hotel_id_room_id_end_date': needed in a foreign key constraint
      await queryInterface.sequelize.query(
        'ALTER TABLE roomPrice DROP INDEX hotel_id_room_id_end_date',
        { transaction }
      );
      await queryInterface.sequelize.query('ALTER TABLE room DROP INDEX hotel_id_room_number', {
        transaction
      });
      await queryInterface.sequelize.query('ALTER TABLE hotel DROP INDEX hotel_name_address', {
        transaction
      });

      transaction.commit();
    } catch (e) {
      console.error(e);
      transaction.rollback();
    }
  }
};
